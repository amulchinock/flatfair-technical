import Vue from 'vue'
import App from './App.vue'
import router from './router'
import VueCurrencyFilter from 'vue-currency-filter'
import VeeValidate from 'vee-validate'

Vue.use(VueCurrencyFilter, {
  symbol: '£',
  thousandsSeparator: ',',
  fractionCount: 2,
  fractionSeparator: '.',
  symbolPosition: 'front',
  symbolSpacing: false
})

Vue.use(VeeValidate, {
  events: '' // disables form validation on change .etc (we only want it to happen on advancing through wizard)
})

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
